module gitlab.com/timeterm/timetable-svc

go 1.12

require (
	github.com/francoispqt/gojay v1.2.13
	github.com/gogo/protobuf v1.3.0
	github.com/joho/godotenv v1.3.0
	github.com/rs/zerolog v1.15.0
	gitlab.com/timeterm/env v0.0.0-20191001061817-01717c815dce
	gitlab.com/timeterm/proto/go v0.0.0-20191008102224-95aa5b974edb
	golang.org/x/net v0.0.0-20191007182048-72f939374954 // indirect
	golang.org/x/sys v0.0.0-20191008105621-543471e840be // indirect
	golang.org/x/text v0.3.2 // indirect
	google.golang.org/genproto v0.0.0-20191007204434-a023cd5227bd // indirect
	google.golang.org/grpc v1.24.0
)
