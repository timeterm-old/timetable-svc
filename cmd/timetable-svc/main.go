package main

import (
	"gitlab.com/timeterm/timetable-svc/cmd/timetable-svc/app"

	_ "github.com/joho/godotenv/autoload"
)

func main() {
	app.Execute()
}
