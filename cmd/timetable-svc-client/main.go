package main

import (
	"gitlab.com/timeterm/timetable-svc/cmd/timetable-svc-client/app"

	_ "github.com/joho/godotenv/autoload"
)

func main() {
	app.Execute()
}
