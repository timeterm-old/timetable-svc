package app

import (
	"log"

	"google.golang.org/grpc"
)

func Execute() {
	conn, err := grpc.Dial("localhost:8080", grpc.WithInsecure())
	if err != nil {
		log.Fatalf("Could not dial server: %v", err)
	}
	defer func() {
		err = conn.Close()
		if err != nil {
			log.Fatalf("Could not close connection: %v", err)
		}
	}()
}
