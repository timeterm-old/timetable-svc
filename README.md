# timetable-svc

## Projectstructuur

Dit project houdt zich zoveel mogelijk aan [golang-standards/project-layout](https://github.com/golang-standards/project-layout)
als mogelijk. Neem dat als referentiepunt als het volgende niet duidelijk maakt waar iets hoort.

```
gitlab.com/timeterm/timetable-svc
  cmd/             - Uitvoerbare bestanden
    timetable-svc/
      app/         - Code voor het uitvoerbare bestand
      main.go      - Ingangspunt voor het uitvoerbare bestand
  pkg/             - Code die ook door andere programma's geïmporteerd kan worden (zonder problemen)           
  go.mod           - Dependencies
  go.sum           - Dependency sum (voor check)
```