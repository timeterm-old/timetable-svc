GOARCH ?= amd64

.PHONY: ci-setup
ci-setup: 
	git config --global url."https://gitlab-ci-token:${CI_JOB_TOKEN}@gitlab.com/".insteadOf "https://gitlab.com/"
	mkdir -p ${CI_PROJECT_DIR}/.cache
	export GOPATH=${CI_PROJECT_DIR}/.cache

.PHONY: build-timetable-svc-linux
build-timetable-svc-linux:
	GOOS=linux $(MAKE) build-timetable-svc

.PHONY: build-timetable-svc
build-timetable-svc:
	CGO_ENABLED=0 installsuffix=cgo go build -ldflags "-extldflags '-static'" -o ./cmd/timetable-svc/timetable-svc-$(GOOS)-$(GOARCH) ./cmd/timetable-svc
	strip --strip-all ./cmd/timetable-svc/timetable-svc-$(GOOS)-$(GOARCH)

.PHONY: docker-timetable-svc-linux
docker-timetable-svc-linux: build-timetable-svc-linux
	docker build cmd/timetable-svc