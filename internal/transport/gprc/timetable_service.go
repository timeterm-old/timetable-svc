package grpctx

import (
	"context"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"github.com/gogo/protobuf/types"
	pb "gitlab.com/timeterm/proto/go/timetable"
	userpb "gitlab.com/timeterm/proto/go/user"
	"gitlab.com/timeterm/timetable-svc/internal/service"
	"gitlab.com/timeterm/timetable-svc/internal/transport/gprc/convert"
	"gitlab.com/timeterm/timetable-svc/pkg/zermelo"
)

type TimetableServer struct {
	s *service.Base
}

type sessInfo struct {
	sess     *userpb.Session
	userCode string
}

func (t TimetableServer) Appointments(ctx context.Context,
	req *pb.AppointmentsRequest,
) (*pb.AppointmentsResponse, error) {
	info, err := t.getSessInfo(ctx, req.GetTtSessionId())
	if err != nil {
		return nil, err
	}

	start, err := types.TimestampFromProto(req.Start)
	if err != nil {
		return nil, err
	}

	end, err := types.TimestampFromProto(req.End)
	if err != nil {
		return nil, err
	}

	if end.Before(start) {
		return nil, status.Error(codes.InvalidArgument, "start (time) has to be before end (time)")
	}

	appointments, err := t.s.Z.GetAppointments(ctx, &zermelo.AppointmentsRequest{
		Start:   zermelo.TimePtr(start),
		End:     zermelo.TimePtr(end),
		Student: zermelo.StringPtr(info.userCode),
	})
	if err != nil {
		return nil, err
	}

	aptsRsp := &pb.AppointmentsResponse{
		Appointments: make([]*pb.Appointment, len(appointments)),
	}
	for i := range appointments {
		apt, err := convert.AppointmentZermeloToProto(&appointments[i])
		if err != nil {
			return nil, err
		}

		aptsRsp.Appointments[i] = apt
	}
	return aptsRsp, nil
}

func (t TimetableServer) Announcements(ctx context.Context,
	req *pb.AnnouncementsRequest,
) (*pb.AnnouncementsResponse, error) {
	info, err := t.getSessInfo(ctx, req.GetTtSessionId())
	if err != nil {
		return nil, err
	}

	announcements, err := t.s.Z.GetAnnouncements(ctx, &zermelo.AnnouncementsRequest{
		Current: zermelo.BoolPtr(req.GetCurrent()),
		Student: zermelo.StringPtr(info.userCode),
	})
	if err != nil {
		return nil, err
	}

	rsp := new(pb.AnnouncementsResponse)
	rsp.Announcements = make([]*pb.Announcement, len(announcements))
	for i := range announcements {
		apt, err := convert.AnnouncementZermeloToProto(&announcements[i])
		if err != nil {
			return nil, err
		}

		rsp.Announcements[i] = apt
	}
	return rsp, nil
}

func (t *TimetableServer) getSessInfo(ctx context.Context, sessID string) (sessInfo, error) {
	sess, err := t.validateSession(ctx, sessID)
	if err != nil {
		return sessInfo{}, err
	}

	userCode, err := t.getUserCode(ctx, sess.GetCardId())
	if err != nil {
		return sessInfo{}, err
	}

	return sessInfo{
		sess:     sess,
		userCode: userCode,
	}, nil
}

func (t *TimetableServer) getUserCode(ctx context.Context, cardID uint32) (string, error) {
	codeRsp, err := t.s.UC.GetUserCode(ctx, &userpb.UserCodeRequest{
		CardId: cardID,
	})
	if err != nil {
		return "", status.Error(codes.Internal, "could not get user code from card ID")
	}
	return codeRsp.GetUserCode(), nil
}
