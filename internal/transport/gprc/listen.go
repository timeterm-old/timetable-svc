package grpctx

import (
	"net"

	"google.golang.org/grpc"

	"gitlab.com/timeterm/env"
	pb "gitlab.com/timeterm/proto/go/timetable"
	"gitlab.com/timeterm/timetable-svc/internal/service"
)

const envListenAddr = "LISTEN_ADDR"

func init() {
	env.RegisterSpec(ListenEnvSpec())
}

func Listen(b *service.Base) error {
	envs, err := ListenEnvSpec().Resolve()
	if err != nil {
		return err
	}

	lis, err := net.Listen("tcp", envs[envListenAddr])
	if err != nil {
		return err
	}

	grpcServer := grpc.NewServer()
	pb.RegisterTimetableServer(grpcServer, TimetableServer{b})

	return grpcServer.Serve(lis)
}

func ListenEnvSpec() env.Spec {
	return env.Spec{
		env.Var{
			Name:     envListenAddr,
			Value:    ":8080",
			Required: true,
		},
	}
}
