package convert

import (
	"time"

	"github.com/gogo/protobuf/types"
	pb "gitlab.com/timeterm/proto/go/timetable"
	"gitlab.com/timeterm/timetable-svc/pkg/zermelo"
)

func AppointmentZermeloToProto(a *zermelo.Appointment) (*pb.Appointment, error) {
	startProto, err := types.TimestampProto(time.Time(a.Start))
	if err != nil {
		return nil, err
	}

	endProto, err := types.TimestampProto(time.Time(a.End))
	if err != nil {
		return nil, err
	}

	createdProto, err := types.TimestampProto(time.Time(a.Created))
	if err != nil {
		return nil, err
	}

	lastModifiedProto, err := types.TimestampProto(time.Time(a.LastModified))
	if err != nil {
		return nil, err
	}

	return &pb.Appointment{
		AppointmentInstance: a.AppointmentInstance,
		Id:                  a.ID,
		Start:               startProto,
		End:                 endProto,
		Created:             createdProto,
		LastModified:        lastModifiedProto,
		ChangeDescription:   a.ChangeDescription,
		StartTimeSlot:       a.StartTimeSlot,
		EndTimeSlot:         a.EndTimeSlot,
		Subjects:            a.Subjects,
		Type:                string(a.Type),
		Remark:              a.Remark,
		Valid:               a.Valid,
		Hidden:              a.Hidden,
		Canceled:            a.Canceled,
		Modified:            a.Modified,
		Moved:               a.Moved,
		New:                 a.New,
		Locations:           a.Locations,
		Teachers:            a.Teachers,
		Groups:              a.Groups,
	}, nil
}
