package convert

import (
	"time"

	"github.com/gogo/protobuf/types"
	pb "gitlab.com/timeterm/proto/go/timetable"
	"gitlab.com/timeterm/timetable-svc/pkg/zermelo"
)

func AnnouncementZermeloToProto(a *zermelo.Announcement) (*pb.Announcement, error) {
	startProto, err := types.TimestampProto(time.Time(a.Start))
	if err != nil {
		return nil, err
	}

	endProto, err := types.TimestampProto(time.Time(a.End))
	if err != nil {
		return nil, err
	}

	return &pb.Announcement{
		Id:    a.ID,
		Start: startProto,
		End:   endProto,
		Title: a.Title,
		Text:  a.Text,
	}, nil
}
