package grpctx

import (
	"context"

	"github.com/rs/zerolog/log"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	userpb "gitlab.com/timeterm/proto/go/user"
)

// validateSession checks if a session is valid using the user-svc.
func (t TimetableServer) validateSession(ctx context.Context, sessID string) (*userpb.Session, error) {
	validationRsp, err := t.s.UC.ValidateSession(ctx, &userpb.ValidateSessionRequest{
		SessionId: sessID,
	})
	if err != nil {
		log.Error().Err(err).Msg("could not reach user-svc trying to validate session")
		return nil, status.Error(codes.Internal, "could not reach account management service")
	}
	if !validationRsp.GetValid() {
		return nil, status.Error(codes.PermissionDenied, "invalid session")
	}
	return validationRsp.GetSession(), nil
}
