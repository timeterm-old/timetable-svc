package zermelo

import (
	"gitlab.com/timeterm/env"
	"gitlab.com/timeterm/timetable-svc/pkg/zermelo"
)

const (
	envZermeloInstitution = "ZERMELO_INSTITUTION"
	envZermeloToken       = "ZERMELO_TOKEN" //nolint:gosec
)

func init() {
	env.RegisterSpec(NewClientEnvSpec())
}

func NewClient() (*zermelo.Client, error) {
	envs, err := NewClientEnvSpec().Resolve()
	if err != nil {
		return nil, err
	}

	return zermelo.NewClient(
		envs[envZermeloInstitution],
		envs[envZermeloToken],
	)
}

func NewClientEnvSpec() env.Spec {
	return env.Spec{
		{
			Name:     envZermeloInstitution,
			Required: true,
		},
		{
			Name:     envZermeloToken,
			Required: true,
		},
	}
}
