package zermelo

import (
	"bytes"
	"context"
	"errors"
	"fmt"
	"net/http"
	"net/url"

	"github.com/francoispqt/gojay"
)

type accessTokenResponse struct {
	AccessToken string `json:"access_token"`
}

func (a *accessTokenResponse) UnmarshalJSONObject(dec *gojay.Decoder, key string) error {
	if key == "access_token" {
		return dec.String(&a.AccessToken)
	}
	return nil
}

func (a *accessTokenResponse) NKeys() int {
	return 1
}

func GetToken(ctx context.Context, institution, authCode string) (string, error) {
	data := url.Values{"grant_type": {"authorization_code"}, "code": {authCode}}.Encode()

	base, err := newBaseURL(institution)
	if err != nil {
		return "", err
	}

	u := base.ResolveReference(&url.URL{Path: "oauth/token"})

	req, err := http.NewRequestWithContext(ctx,
		http.MethodPost,
		u.String(),
		bytes.NewBuffer([]byte(data)),
	)
	if err != nil {
		return "", fmt.Errorf("could not create request: %w", err)
	}
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")

	rsp, err := http.DefaultClient.Do(req)
	if err != nil {
		return "", fmt.Errorf("could not do request: %w", err)
	}
	defer closeResponseBody(rsp.Body)

	if rsp.StatusCode < 200 || rsp.StatusCode >= 400 {
		return "", errors.New("incorrect authorization authCode")
	}

	var rspData accessTokenResponse
	err = gojay.NewDecoder(rsp.Body).DecodeObject(&rspData)
	if err != nil {
		return "", fmt.Errorf("could not decode response: %w", err)
	}

	return rspData.AccessToken, nil
}
