package zermelo

import (
	"reflect"
	"strings"
)

func jsonFields(v interface{}) []string {
	return jsonFieldsReflect(reflect.TypeOf(v))
}

func jsonFieldsReflect(t reflect.Type) []string {
	switch t.Kind() {
	case reflect.Ptr:
		return jsonFieldsReflect(t.Elem())
	case reflect.Struct:
	default:
		return nil
	}

	numField := t.NumField()
	fields := make([]string, 0, numField)

	for i := 0; i < numField; i++ {
		v := t.Field(i)
		jsonTag := v.Tag.Get("json")
		parts := strings.Split(jsonTag, ",")
		if len(parts) > 0 {
			fields = append(fields, parts[0])
		}
	}

	return fields
}
