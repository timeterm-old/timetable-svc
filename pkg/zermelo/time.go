package zermelo

import (
	"strconv"
	"time"
)

type Time time.Time

func (t Time) MarshalText() ([]byte, error) {
	return []byte(strconv.FormatInt(time.Time(t).Unix(), 10)), nil
}

func (t *Time) UnmarshalText(data []byte) error {
	unix, err := strconv.ParseInt(string(data), 10, 64)
	if err != nil {
		return err
	}
	*t = Time(time.Unix(unix, 0))
	return nil
}

func (t Time) MarshalJSON() ([]byte, error) {
	return t.MarshalText()
}

func (t *Time) UnmarshalJSON(data []byte) error {
	return t.UnmarshalText(data)
}
