package zermelo

import "net/http"

type RequestOpt func(r *http.Request)

func WithUser(user string) RequestOpt {
	return func(r *http.Request) {
		r.URL.Query().Set("user", user)
	}
}
