package zermelo

import (
	"context"
	"errors"
	"fmt"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"time"

	"github.com/rs/zerolog/log"

	"github.com/francoispqt/gojay"
)

type AppointmentType string

const (
	AtUnknown  AppointmentType = "unknown"
	AtLesson   AppointmentType = "lesson"
	AtExam     AppointmentType = "exam"
	AtActivity AppointmentType = "activity"
	AtChoice   AppointmentType = "choice"
	AtTalk     AppointmentType = "talk"
	AtOther    AppointmentType = "other"
)

func (a AppointmentType) Valid() bool {
	switch a {
	case AtUnknown:
	case AtLesson:
	case AtExam:
	case AtActivity:
	case AtChoice:
	case AtTalk:
	case AtOther:
	default:
		return false
	}
	return true
}

var appointmentFields = jsonFields(Appointment{})

// A Zermelo appointment.
// See https://confluence.zermelo.nl/display/DEV/Appointment.
type Appointment struct {
	AppointmentInstance int64           `json:"appointmentInstance"`
	ID                  int64           `json:"id"`
	Start               Time            `json:"start"`
	End                 Time            `json:"end"`
	StartTimeSlot       int32           `json:"startTimeSlot"`
	EndTimeSlot         int32           `json:"endTimeSlot"`
	Subjects            []string        `json:"subjects"`
	Type                AppointmentType `json:"type"`
	Remark              string          `json:"remark"`
	Locations           []string        `json:"locations"`
	Teachers            []string        `json:"teachers"`
	Groups              []string        `json:"groups"`
	Created             Time            `json:"created"`
	LastModified        Time            `json:"lastModified"`
	Valid               bool            `json:"valid"`
	Hidden              bool            `json:"hidden"`
	Canceled            bool            `json:"cancelled"` //nolint:misspell
	Modified            bool            `json:"modified"`
	Moved               bool            `json:"moved"`
	New                 bool            `json:"new"`
	ChangeDescription   string          `json:"changeDescription"`
	Branch              string          `json:"branch"`
}

const (
	aptKeyAppointmentInstance = "appointmentInstance"
	aptKeyID                  = "id"
	aptKeyStart               = "start"
	aptKeyEnd                 = "end"
	aptKeyStartTimeSlot       = "startTimeSlot"
	aptKeyEndTimeSlot         = "endTimeSlot"
	aptKeySubjects            = "subjects"
	aptKeyType                = "type"
	aptKeyRemark              = "remark"
	aptKeyLocations           = "locations"
	aptKeyTeachers            = "teachers"
	aptKeyGroups              = "groups"
	aptKeyCreated             = "created"
	aptKeyLastModified        = "lastModified"
	aptKeyValid               = "valid"
	aptKeyHidden              = "hidden"
	aptKeyCanceled            = "cancelled" //nolint:misspell
	aptKeyModified            = "modified"
	aptKeyMoved               = "moved"
	aptKeyNew                 = "new"
	aptKeyChangeDescription   = "changeDescription"
	aptKeyBranch              = " branch"
)

func (a *Appointment) UnmarshalJSONObject(dec *gojay.Decoder, key string) error { //nolint:gocyclo
	switch key {
	case aptKeyAppointmentInstance:
		return dec.Int64(&a.AppointmentInstance)
	case aptKeyID:
		return dec.Int64(&a.ID)
	case aptKeyStart:
		return decodeTime(&a.Start, dec)
	case aptKeyEnd:
		return decodeTime(&a.End, dec)
	case aptKeyStartTimeSlot:
		return dec.Int32(&a.StartTimeSlot)
	case aptKeyEndTimeSlot:
		return dec.Int32(&a.EndTimeSlot)
	case aptKeySubjects:
		return dec.SliceString(&a.Subjects)
	case aptKeyType:
		return dec.String((*string)(&a.Type))
	case aptKeyRemark:
		return dec.String(&a.Remark)
	case aptKeyLocations:
		return dec.SliceString(&a.Locations)
	case aptKeyTeachers:
		return dec.SliceString(&a.Teachers)
	case aptKeyGroups:
		return dec.SliceString(&a.Groups)
	case aptKeyCreated:
		return decodeTime(&a.Created, dec)
	case aptKeyLastModified:
		return decodeTime(&a.LastModified, dec)
	case aptKeyValid:
		return dec.Bool(&a.Valid)
	case aptKeyHidden:
		return dec.Bool(&a.Hidden)
	case aptKeyCanceled:
		return dec.Bool(&a.Canceled)
	case aptKeyModified:
		return dec.Bool(&a.Modified)
	case aptKeyMoved:
		return dec.Bool(&a.Moved)
	case aptKeyNew:
		return dec.Bool(&a.New)
	case aptKeyChangeDescription:
		return dec.String(&a.ChangeDescription)
	case aptKeyBranch:
		return dec.String(&a.Branch)
	}
	return nil
}

func (a *Appointment) NKeys() int {
	return 22
}

type Appointments []Appointment

func (a *Appointments) UnmarshalJSONArray(dec *gojay.Decoder) error {
	var apt Appointment
	if err := dec.Object(&apt); err != nil {
		return err
	}
	*a = append(*a, apt)
	return nil
}

func decodeTime(t *Time, dec *gojay.Decoder) error {
	var secs int64
	err := dec.Int64(&secs)
	if err != nil {
		return err
	}
	*t = Time(time.Unix(secs, 0))
	return nil
}

type AppointmentsRequest struct {
	// Required.
	Start *Time
	End   *Time

	// Choose either.
	Student *string
	Teacher *string

	Valid         *bool
	Canceled      *bool
	IncludeHidden *bool
	ModifiedSince *Time
}

func (ar AppointmentsRequest) addToRequest(r *http.Request) error {
	q := r.URL.Query()

	if ar.Start == nil {
		return errors.New("field Start needs to have a non-nil value")
	}
	if ar.End == nil {
		return errors.New("field End needs to have a non-nil value")
	}
	startText, _ := ar.Start.MarshalText()
	endText, _ := ar.End.MarshalText()
	q.Set(aptKeyStart, string(startText))
	q.Set(aptKeyEnd, string(endText))

	if ar.Valid != nil {
		q.Set(aptKeyValid, strconv.FormatBool(*ar.Valid))
	}
	if ar.Canceled != nil {
		q.Set(aptKeyCanceled, strconv.FormatBool(*ar.Canceled)) //nolint:misspell
	}
	if ar.IncludeHidden != nil {
		q.Set("includeHidden", strconv.FormatBool(*ar.IncludeHidden))
	}
	if ar.Student != nil {
		q.Set("possibleStudents", *ar.Student)
	}
	if ar.Teacher != nil {
		q.Set("teacher", *ar.Teacher)
	}
	if ar.ModifiedSince != nil {
		text, err := ar.ModifiedSince.MarshalJSON()
		if err != nil {
			return fmt.Errorf("could not marshal ModifiedSince: %w", err)
		}
		r.URL.Query().Set("modifiedSince", string(text))
	}

	r.URL.RawQuery = q.Encode()

	return nil
}

func (c *Client) GetAppointments(ctx context.Context,
	r *AppointmentsRequest,
	opts ...RequestOpt,
) (Appointments, error) {
	req, err := http.NewRequestWithContext(ctx, http.MethodGet,
		c.base.ResolveReference(&url.URL{Path: "appointments"}).String(),
		nil,
	)
	if err != nil {
		return nil, fmt.Errorf("could not create request: %w", err)
	}

	q := req.URL.Query()
	q.Set("fields", strings.Join(appointmentFields, ","))
	req.URL.RawQuery = q.Encode()

	err = r.addToRequest(req)
	if err != nil {
		return nil, err
	}

	rsp, err := c.do(req, opts...)
	if err != nil {
		return nil, fmt.Errorf("could not do request: %w", err)
	}
	defer closeResponseBody(rsp.Body)

	if rsp.StatusCode < 200 || rsp.StatusCode >= 400 {
		log.Error().Msgf("appointments request to Zermelo was not successful: got status code %d", rsp.StatusCode)
		return nil, fmt.Errorf("request to Zermelo was not successful: got status code %d", rsp.StatusCode)
	}

	var appointments Appointments
	err = decodeResponse(rsp.Body, &appointments)
	return appointments, err
}
