package zermelo

import "time"

func TimePtr(t time.Time) *Time {
	return (*Time)(&t)
}

func BoolPtr(b bool) *bool {
	return &b
}

func StringPtr(s string) *string {
	return &s
}
