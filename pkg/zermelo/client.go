package zermelo

import (
	"bytes"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"runtime"

	"github.com/rs/zerolog/log"
)

type Client struct {
	token string
	base  *url.URL
}

func NewClient(institution, token string) (*Client, error) {
	base, err := newBaseURL(institution)
	if err != nil {
		return nil, fmt.Errorf("error creating base url: %w", err)
	}

	return &Client{
		token: token,
		base:  base,
	}, nil
}

func newBaseURL(institution string) (*url.URL, error) {
	var b bytes.Buffer

	b.WriteString("https://")
	b.WriteString(institution)
	b.WriteString(".zportal.nl/api/v3/")

	return url.Parse(b.String())
}

func (c *Client) do(r *http.Request, opts ...RequestOpt) (*http.Response, error) {
	r.Header.Set("Authorization", "Bearer "+c.token)

	for _, opt := range opts {
		opt(r)
	}

	return http.DefaultClient.Do(r)
}

func closeResponseBody(c io.Closer) {
	if err := c.Close(); err != nil {
		msg := "Could not close response body"

		pc, _, _, ok := runtime.Caller(1)
		details := runtime.FuncForPC(pc)
		if !ok && details != nil {
			msg += fmt.Sprintf(" (provided by %v)", details.Name())
		}

		log.Error().Err(err).Msg(msg)
	}
}
