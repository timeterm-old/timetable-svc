package zermelo

import (
	"context"
	"fmt"
	"net/http"
	"net/url"
	"strconv"
	"strings"

	"github.com/rs/zerolog/log"

	"github.com/francoispqt/gojay"
)

var announcementFields = jsonFields(Announcement{})

type Announcement struct {
	ID    int64  `json:"id"`
	Start Time   `json:"start"`
	End   Time   `json:"end"`
	Title string `json:"title"`
	Text  string `json:"text"`
}

func (a *Announcement) UnmarshalJSONObject(dec *gojay.Decoder, key string) error {
	switch key {
	case "id":
		return dec.Int64(&a.ID)
	case "start":
		return decodeTime(&a.Start, dec)
	case "end":
		return decodeTime(&a.End, dec)
	case "title":
		return dec.String(&a.Title)
	case "text":
		return dec.String(&a.Text)
	}
	return nil
}

func (a *Announcement) NKeys() int {
	return 5
}

type Announcements []Announcement

func (a *Announcements) UnmarshalJSONArray(dec *gojay.Decoder) error {
	var ann Announcement
	if err := dec.DecodeObject(&ann); err != nil {
		return err
	}
	*a = append(*a, ann)
	return nil
}

type AnnouncementsRequest struct {
	Current *bool
	Student *string
}

func (ar AnnouncementsRequest) addToRequest(r *http.Request) {
	q := r.URL.Query()
	if ar.Current != nil {
		q.Set("current", strconv.FormatBool(*ar.Current))
	}
	if ar.Student != nil {
		q.Set("~me", *ar.Student)
	}
	r.URL.RawQuery = q.Encode()
}

func (c *Client) GetAnnouncements(ctx context.Context,
	r *AnnouncementsRequest,
	opts ...RequestOpt,
) ([]Announcement, error) {
	req, err := http.NewRequestWithContext(ctx, http.MethodGet,
		c.base.ResolveReference(&url.URL{Path: "announcements"}).String(),
		nil,
	)
	if err != nil {
		return nil, fmt.Errorf("could not create request: %w", err)
	}

	r.addToRequest(req)

	q := req.URL.Query()
	q.Set("fields", strings.Join(announcementFields, ","))
	req.URL.RawQuery = q.Encode()

	rsp, err := c.do(req, opts...)
	if err != nil {
		return nil, fmt.Errorf("could not do request: %w", err)
	}
	defer closeResponseBody(rsp.Body)

	if rsp.StatusCode < 200 || rsp.StatusCode >= 400 {
		log.Error().Msgf("announcements request to Zermelo was not successful: got status code %d", rsp.StatusCode)
		return nil, fmt.Errorf("request to Zermelo was not successful: got status code %d", rsp.StatusCode)
	}

	var announcements Announcements
	err = decodeResponse(rsp.Body, &announcements)
	return announcements, err
}
