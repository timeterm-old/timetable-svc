package zermelo

import (
	"encoding/json"
	"fmt"
	"io"
	"reflect"

	"github.com/rs/zerolog/log"

	"github.com/francoispqt/gojay"
)

type responseInner struct {
	Status    int32           `json:"status"`
	Message   string          `json:"message"`
	StartRow  int32           `json:"startRow"`
	EndRow    int32           `json:"endRow"`
	TotalRows int32           `json:"totalRows"`
	Data      json.RawMessage `json:"data"`
}

func (r *responseInner) UnmarshalJSONObject(dec *gojay.Decoder, key string) error {
	switch key {
	case "status":
		return dec.Int32(&r.Status)
	case "message":
		return dec.String(&r.Message)
	case "startRow":
		return dec.Int32(&r.StartRow)
	case "endRow":
		return dec.Int32(&r.EndRow)
	case "totalRows":
		return dec.Int32(&r.TotalRows)
	case "data":
		if r.Data != nil && !reflect.ValueOf(r.Data).IsNil() {
			return dec.Decode(r.Data)
		}
	}
	return nil
}

func (r *responseInner) NKeys() int {
	return 6
}

type response struct {
	Inner responseInner `json:"response"`
}

func decodeResponse(r io.Reader, inner interface{}) error {
	rsp := response{}
	err := json.NewDecoder(r).Decode(&rsp)
	if err != nil {
		return fmt.Errorf("could not decode response: %w", err)
	}

	if rsp.Inner.Status < 200 || rsp.Inner.Status >= 400 {
		log.Error().Msgf("announcements request to Zermelo was not successful: got status code %d", rsp.Inner.Status)
		return fmt.Errorf("request to Zermelo was not successful: got status code %d", rsp.Inner.Status)
	}

	err = json.Unmarshal(rsp.Inner.Data, inner)
	if err != nil {
		return fmt.Errorf("could not decode response inner: %w", err)
	}
	return nil
}

func (r *response) UnmarshalJSONObject(dec *gojay.Decoder, key string) error {
	if key == "response" {
		return dec.Object(&r.Inner)
	}
	return nil
}

func (r *response) NKeys() int {
	return 1
}
